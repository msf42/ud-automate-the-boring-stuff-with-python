#! /usr/bin/python3

import re, pyperclip

# Create a regex for phone numbers
phoneRegex = re.compile(r'''
(
# 415-555-0000, 222-2222, (543) 333-3333, 222-3333 ext 12345, ext.22222, x23433
((\d\d\d) | (\(\d\d\d\)))?        # area code (optional)
(\s|-)              # first separator
\d\d\d              # first 3 digits
-                   # separator
\d\d\d\d            # last 4 digits      
(((ext(\.)?\s) |x)  # extension word (optional)
(\d{2,5}))?         # extension number (optional)
)
''', re.VERBOSE)

# Create a regex for email addresses
emailRegex = re.compile(r'''
[a-zA-Z0-9_.+]+          # name part
@                        # symbol
[a-zA-Z0-9_.+]+          # domain name   
''', re.VERBOSE)


# Get the text off the clipboard
text = pyperclip.paste()

# TODO: Extract teh email/phone from the text
extractedPhone = phoneRegex.findall(text)
extractedEmail = emailRegex.findall(text)

allPhoneNumbers = []
for phoneNumber in extractedPhone:
    allPhoneNumbers.append(phoneNumber[0])

# TODO: Copy the extracted email/phone to the clipboard
results = '\n'.join(allPhoneNumbers) + '\n' + '\n'.join(extractedEmail)

pyperclip.copy(results)