# Automate the Boring Stuff with Python

These are my notes from Udemy [Automate the Boring Stuff with Python](https://www.udemy.com/course/automate/) on [Udemy](www.udemy.com).

This was a nice addition to the Python Bootcamp course.

![Completion Certificate](Python_ATBSWP.jpg)
