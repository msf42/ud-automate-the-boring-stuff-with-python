# Section 11: Files

## 30: Filenames and Relative/Absolute Paths

```py
import os
os.path.join('folder1', 'folder2', 'file1.jpg')

Out[1]: 'folder1/folder2/file1.jpg'

os.sep
Out[2]: '/'

os.getcwd()
Out[3]: '/home/steve'

os.chdir('/home/steve/Dropbox')

os.getcwd()
Out[5]: '/home/steve/Dropbox'

# . = means this directory

# .. = parent folder

os.chdir('..')

os.getcwd()
Out[9]: '/home/steve'

os.path.abspath
Out[10]: <function posixpath.abspath>

os.path.abspath()
Traceback (most recent call last):

  File "<ipython-input-11-e24dae7b72ec>", line 1, in <module>
  os.path.abspath()

TypeError: abspath() missing 1 required positional argument: 'path'

# os.path.abspath(file) returns abs path

# os.path.isabspath(path) returnts boolean

# os.path.relpath('path1', 'path2') gives a relative path from the first location to the second

# os.path.dirname() retrieves directory part

# os.path.basename() retrieves last part (file.png)

# os.path.exists('path') returns boolean

# os.path.isfile() or os.path.isdir()

# os.path.getsize(/home/steve/Dropbox)
```

---

## 31: Reading and Writing Plain Text Files

```py
helloFile = open('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python - Automate Boring Stuff/file1.txt')

helloFile.read() # can only read once, as a single string

helloFile = open('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python - Automate Boring Stuff/file1.txt')
content = helloFile.read() # assigning it to a variable

helloFile = open('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python - Automate Boring Stuff/file1.txt')
helloFile.readlines() # list of strings
helloFile.close()

helloFile = open('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python - Automate Boring Stuff/file1.txt', 'w') # write mode
helloFile = open('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python - Automate Boring Stuff/file1.txt', 'a') # append mode

helloFile = open('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python - Automate Boring Stuff/file2.txt', 'w')
helloFile.write('Hello!!!!!')
helloFile.close()

baconFile = open('bacon.txt','w') # saved in cwd
baconFile.write('Bacon is not a vegetable')
baconFile.close()


import shelve # saves a binary file that you can access later
shelfFile = shelve.open('mydata')
shelfFile['cats'] = ['Zophie', 'Pooka', 'Simon']
shelfFile.close()

shelfFile = shelve.open('mydata')
shelfFile['cats']
# ['Zophie', 'Pooka', 'Simon']
# shelve files are similar to dictionaries
list(shelfFile.keys()) # or values
```

---

## 32: Copying and Moving Files and Folders

```py
import shutil
# copy single file
shutil.copy('from/folder/file.txt', 'to/folder/')
# copy folder
shutil.copytree('folder path from', 'folder path to')
# move
shutil.move('from/folder/file.txt', 'new/location')
# for renaming, use move function
# move to same folder with new name
```

---

## 33: Deleting Files

```py
import os
os.unlink()
# deletes a single file

os.rmdir()
# deletes an empty folder

import shutil
shutil.rmtree(path)
# removes folder and contents

# DRY RUN
# comment out the dangerous code, and replace it with print
for filename in os.listdir():
  if filename.endswith('.txt'):
    # os.unlink(filename)
    print(filename)

import send2trash
send2trash.send2trash()
# sends file to trash
```

---

## 34: Walking a Directory Tree

```py
import os
for folderName, subfolders, filenames os.walk('root folder name'):
  # first iteration goes through root folder
  # next iteration goes through next level, and so on
```

---
