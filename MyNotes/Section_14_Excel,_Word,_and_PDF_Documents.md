# Section 14: Excel, Word, and PDF Documents

## 42: Reading Excel Spreadsheets

```py
In [1]: import openpyxl

In [2]: import os

In [3]: os.chdir('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python-AutomateTheBoringStuff')

In [4]: workbook = openpyxl.load_workbook('example.xlsx')

In [5]: type(workbook)
Out[5]: openpyxl.workbook.workbook.Workbook

In [6]: sheet = workbook.get_sheet_by_name('Sheet1')

In [7]: workbook.get_sheet_names()
Out[7]: ['Sheet1', 'Sheet2', 'Sheet3']

In [8]: sheet['A1']
Out[8]: <Cell 'Sheet1'.A1>

In [10]: cell = sheet['A1']

In [11]: cell.value
Out[11]: datetime.datetime(2015, 4, 5, 13, 34, 2)

In [13]: str(cell.value)
Out[13]: '2015-04-05 13:34:02'

In [14]: sheet.cell(row=1, column=2)
Out[14]: <Cell 'Sheet1'.B1>
```

----------

## 43: Editing Excel Spreadsheets

```py
In [1]: import openpyxl

In [2]: import os

In [3]: os.chdir('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python-AutomateTheBoringStuff')

In [4]: wb = openpyxl.Workbook()

In [5]: wb
Out[5]: <openpyxl.workbook.workbook.Workbook at 0x7fd5389bc470>

In [6]: wb.get_sheet_names()
Out[6]: ['Sheet']

In [7]: sheet = wb.get_sheet_by_name('Sheet')

In [8]: sheet
Out[8]: <Worksheet "Sheet">

In [9]: sheet['A1'].value

In [10]: sheet['A1'] = 42

In [11]: sheet['A2'] = 'hello'

In [12]: wb.save('example.xlsx')

In [13]: sheet2 = wb.create_sheet()

In [14]: wb.get_sheet_names()
Out[14]: ['Sheet', 'Sheet1']

In [15]: sheet2.title = 'My New Sheet'

In [16]: wb.get_sheet_names()
Out[16]: ['Sheet', 'My New Sheet']

In [17]: wb.save('example2.xlsx')

In [18]: wb.create_sheet(index=0, title='other sheet')
Out[18]: <Worksheet "other sheet">

In [19]: wb.save('example3.xlsx')

In [20]: wb.get_sheet_names()
Out[20]: ['other sheet', 'Sheet', 'My New Sheet']
```

----------

## 44: Reading and Editing PDFs

### Can’t do much with PDFs

```py
In [2]: import PyPDF2

In [3]: import os

In [4]: os.chdir('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python-AutomateTheBoringStuff')

In [5]: pdfFile = open('meetingminutes1.pdf')

In [6]: pdfFile = open('meetingminutes1.pdf', 'rb')

In [7]: PyPDF2.PdfFileReader(pdfFile)
Out[7]: <PyPDF2.pdf.PdfFileReader at 0x7f75d4526748>

In [8]: reader = PyPDF2.PdfFileReader(pdfFile)

In [9]: reader.numPages
Out[9]: 19

In [10]: page = reader.getPage(0)

In [11]: page.extractText()
Out[11]: 'OOFFFFIICCIIAALL BBOOAARRDD MMIINNUUTTEESS Meeting of \nMarch 7\n, 2014\n \n The Board of Elementary and Secondary Education shall provide leadership and \ncreate policies for education that expand opportunities for children, empower \nfamilies and communities, and advance Louisiana in an increasingly \ncompetitive glob\nal market.\n BOARD \n of ELEMENTARY\n and \n SECONDARY\n EDUCATION\n '
```

----------

## 45: Reading and Editing Word Files

```py
In [1]: import docx

In [2]: import os

In [3]: os.chdir('/home/steve/Dropbox/_ACTIVE/Tech/Python/Python-AutomateTheBoringStuff')

In [4]: docx.Document('demo.docx')
Out[4]: <docx.document.Document at 0x7f1b3c2429d8>

In [5]: d = docx.Document('demo.docx')

In [6]: d.paragraphs
Out[6]:
[<docx.text.paragraph.Paragraph at 0x7f1b3c2506d8>,
<docx.text.paragraph.Paragraph at 0x7f1b3c250470>,
<docx.text.paragraph.Paragraph at 0x7f1b3c250320>,
<docx.text.paragraph.Paragraph at 0x7f1b3c250550>,
<docx.text.paragraph.Paragraph at 0x7f1b3c250518>,
<docx.text.paragraph.Paragraph at 0x7f1b3c250358>,
<docx.text.paragraph.Paragraph at 0x7f1b3c250400>]

In [7]: d.paragraphs[0].text
Out[7]: 'Document Title'

In [8]: d.paragraphs[2].text
Out[8]: 'Heading, level 1'

In [9]: p = d.paragraphs[1].text

In [10]: p
Out[10]: 'A plain paragraph having some bold and some italic.'

In [12]: p = d.paragraphs[1]

In [13]: p.runs[0].text
Out[13]: 'A plain paragraph having some '

In [14]: p.runs[0].bold

In [15]: p.runs[1].bold
Out[15]: True

In [16]: p.runs[3].underline = True

In [18]: d.save('demo2.docx')

In [19]: d = docx.Document()

In [20]: d.add_paragraph('Hellooooooo')
Out[20]: <docx.text.paragraph.Paragraph at 0x7f1b3c1ed240>

In [21]: d.add_paragraph('zippitybippitybop')
Out[21]: <docx.text.paragraph.Paragraph at 0x7f1b3c1ed6a0>
```

----------
