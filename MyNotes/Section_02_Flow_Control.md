# Section 02: Flow Control

## 04: Flow Charts and Basic Flow Control Concepts

### Boolean Values

- Only True and False

### Comparison Operators

- `==`
- `!=`
- `<`
- `>`
- `<=`
- `>=`
- While strings and integers can not be equal, integers and floats can.

### Boolean Operator

- `and`
- `or`
- `not`

---

## 05: If, Else, and Elif Statements

- New blocks begin only after statements that end with `:`
- Blank strings have a boolean value of False, all other strings are True
- 0 or 0.0 have a boolean value of False, all other integers and floats are True
- Otherwise, just basics of If/Else/Elif

---

## 06: While Loops

- **Break** causes the program to immediately exit the loop and go to the next line outside the loop.
- **Continue** causes the program to jump back to the start of the loop and reevaluate its condition.
  - I did not quite understand this before now!

---

## 07: For Loops

- Instead of looping as long as a certain condition is met (such as a **while** loop), a **for loop** iterates a specific number of times.
  - So, they are very similar, but a **for loop** is more concise (and safer!)

---
