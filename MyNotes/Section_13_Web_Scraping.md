# Section 13: Web Scraping

## 38: The webbrowser module

### The webbrowser module

Pretty much only does one thing

```py
In [1]: import webbrowser

In [2]: webbrowser.open('http://www.theonion.com')
```

### The script

```py
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 31 17:07:05 2019

@author: steve
"""

import webbrowser, sys, pyperclip

sys.argv # ['mapit.py', '870', 'Valencia', 'St.']

if len(sys.argv) > 1:
  address = ' '.join(sys.argv[1:])
else:
  address = pyperclip.paste()

webbrowser.open('https://www.google.com/maps/place/' + address)
```

### The Command Line

```bash
python3 mapit.py 420 40th St Ste 2, Oakland, CA 94609
```

---

## 39: Downloading From the Web with the requests Module

```py
In [12]: import requests

In [13]: res = requests.get('http://www.automatetheboringstuff.com/files/rj.txt')

In [14]: len(res.text)
Out[14]: 178978

In [15]: print(res.text[:100])
The Project Gutenberg EBook of Romeo and Juliet, by William Shakespeare

This eBook is for the use

In [16]: playFile = open('RomeoAndJuliet.txt', 'wb')

In [17]: for chunk in res.iter_content(100000):
  ...: playFile.write(chunk)
  ...:
# this didn't work
```

---

## 40: Parsing HTML with the Beautiful Soup Module

Simply won’t work. I’ve tried everything.

---

## 41: Controlling the Browser with the Selenium Module

```py
In [1]: from selenium import webdriver

In [2]: browser = webdriver.Chrome()

In [3]: browser.get('https://automatetheboringstuff.com')

In [4]: elem = browser.find_element_by_css_selector('body > div.main > div:nth-child(1) > ul:nth-child(19) > li:nth-child(1) > a')

In [5]: elem
Out[5]: <selenium.webdriver.remote.webelement.WebElement (session="a3a0a775673b64da32560a2052fe24de", element="0.537955660731593-1")>

In [6]: elems = browser.find_elements_by_css_selector('p')

In [7]: len(elems)
Out[7]: 9

In [8]: elem.click()

In [9]: elems = browser.find_elements_by_css_selector('p')

In [10]: len(elems)
Out[10]: 112

In [11]: browser.back()

In [12]: browser.forward()

In [13]: browser.refresh()

In [14]: browser.quit()

In [15]: browser = webdriver.Chrome()

In [16]: browser.get('https://automatetheboringstuff.com')

In [19]: elem = browser.find_element_by_css_selector('body > div.main > div:nth-child(1) > p:nth-child(7)')

In [20]: elem.text
Out[20]: "In Automate the Boring Stuff with Python, you'll learn how to use Python to write programs that do in minutes what would take you hours to do by hand-no prior programming experience required. Once you've mastered the basics of programming, you'll create Python programs that effortlessly perform useful and impressive feats of automation to:"

In [21]: elem = browser.find_element_by_css_selector('html') # to get everything
```

---
