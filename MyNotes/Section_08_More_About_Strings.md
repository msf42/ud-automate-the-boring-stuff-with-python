# Section 08: More About Strings

## 19: Advanced String Syntax

- Single & double quotes, escape characters
- Raw strings - `print(r``'``blah, blah….``'``)` prints everything literally, including \s
- Use `"""` or `'''` to start and finish a multi-line string

```py
spam = 'Hello World'

spam[0]
Out[2]: 'H'

spam[1:5]
Out[3]: 'ello'

spam[-1]
Out[4]: 'd'

'Hell' in spam
Out[5]: True
```

---

## 20: String Methods

- `.upper()` and `.lower()`
- `.title()` will make each word capitalized

These return boolean values

- `.isupper()` and `.islower()`
- `.isalpha()` = letters only
- `.isalnum()` = is alphanumeric only
- `.isdecimal()`= numbers only
- `.isspace()` = whitespace only
- `.istitle()` = titlecase only (each word capitalized)
- `.startswith()` and `endswith()` = string starts or ends with the letter(s) passed
- `.join()` = joins multiple strings
- ‘separator’.join(list of strings)
- `.split()` = splits on whitespace by default
- ‘strint’.split(character)
- `.ljust()` and `.rjust()` = left or right justifies the string within the number of spaces passed to it
- `'``Hello``'``.rjust(10)` will return      `Hello`
- `'Hello'.rjust(10, '*')` will return `*****Hello`
- `.center()` works the same way
- `.strip()`, `.rstrip()`, and `.lstrip()` = removes whitespace by default
- `.replace(``'``x``'``,` `'``y``'``)`  = replace all `x`s with `y`s

### pyperclip module

`pyperclip.copy()` and `pyperclip.paste()` = copy and paste to/from the clipboard

---

## 21: String Formatting

```py
In [3]: 'hello ' + 'world'
Out[3]: 'hello world'

In [4]: name = 'Alice'

In [5]: place = 'Main Street'

In [6]: time = '6 pm'

In [7]: food = 'turnips'

In [9]: 'Hello %s, you are invited to a party at %s at %s. Please bring %s.' % (name, place, time, food)

Out[9]: 'Hello Alice, you are invited to a party at Main Street at 6 pm. Please bring turnips.'
```

---
