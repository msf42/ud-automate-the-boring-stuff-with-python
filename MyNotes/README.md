# Automate the Boring Stuff with Python

- [Section 01: Python Basics](Section_01_Python_Basics.md)

- [Section 02: Flow Control](Section_02_Flow_Control.md)

- [Section 03: Functions](Section_03_Functions.md)

- [Section 04: Handling Errors with Try/Except](Section_04_Handling_Errors_with_Try_Except.md)

- [Section 05: Writing a Complete Program](Section_05_Writing_a_Complete_Program_Guess_the.md)

- [Section 06: Lists](Section_06_Lists.md)

- [Section 07: Dictionaries](Section_07_Dictionaries.md)

- [Section 08: More About Strings](Section_08_More_About_Strings.md)

- [Section 09: Running Programs from the Command Line](Section_09_Running_Programs_from_the_Command_Line.md)

- [Section 10: Regular Expressions](Section_10_Regular_Expressions.md)

- [Section 11: Files](Section_11_Files.md)

- [Section 12: Debugging](Section_12_Debugging.md)

- [Section 13: Web Scraping](Section_13_Web_Scraping.md)

- [Section 14: Excel, Word, and PDF Documents](Section_14_Excel,_Word,_and_PDF_Documents.md)

- [Section 15: Email](Section_15_Email.md)

- [Section 16: GUI Automation](Section_16_GUI_Automation.md)

---
