# Section 12: Debugging

## 35: The Raise and Assert Statements

```py
def boxPrint(symbol, width, height):
  if len(symbol) != 1:
    raise Exception('"symbol" needs to be of length 1')
  if (width < 2) or (height < 2):
    raise Exception('width and height must be greater than 2')
  print(symbol * width)
  for i in range(height-2):
    print(symbol + (' ' * (width-2)) + symbol)

  print(symbol * width)

# traceback.format_exc() Function

import traceback
try:
  raise Exception('This is the error message')
except:
  errorFile = open('error_log.txt', 'a')
  errorFile.write(traceback.format_exc())
  errorFile.close()
  print('The traceback info was written to error_log.txt')

# Assertions

market_2nd = {'ns': 'green', 'ew': 'red'}

def switchLights(intersection):
  for key in intersection.keys():
    if intersection[key] == 'green':
      intersection[key] = 'yellow'
    elif intersection[key] == 'yellow':
      intersection[key] = 'red'
    elif intersection[key] == 'red':
      intersection[key] = 'green'
  assert 'red' in intersection.values(), 'Neither light is red!' + str(intersection)

switchLights(market_2nd)
Traceback (most recent call last):

  File "<ipython-input-18-4995b1994d25>", line 14, in <module>
  switchLights(market_2nd)

  File "<ipython-input-18-4995b1994d25>", line 11, in switchLights
  assert 'red' in intersection.values(), 'Neither light is red!' + str(intersection)

AssertionError: Neither light is red!{'ns': 'yellow', 'ew': 'green'}
```

---

## 36: Logging

### `logging.basicConfig()` function

```py
import logging
logging.basicConfig(filename='myProgramLog.txt', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
logging.debug('Start of program')

def factorial(n):
  logging.debug('Start of factorial(%s)' % (n))
  total = 1
  for i in range(1,n + 1): # added to correct
    total *= i
    logging.debug('i is %s, total is %s' % (i,total))
  logging.debug('return value is %s' % (total))
  return total

factorial(5)

logging.debug('End of program')

logging.disable(logging.CRITICAL) # disables all logging at this level or below
```

### Output of buggy version

```py
2019-05-29 19:45:43,255 - DEBUG - Start of program
2019-05-29 19:45:43,256 - DEBUG - Start of factorial(5)
2019-05-29 19:45:43,257 - DEBUG - i is 0, total is 0
2019-05-29 19:45:43,258 - DEBUG - i is 1, total is 0
2019-05-29 19:45:43,258 - DEBUG - i is 2, total is 0
2019-05-29 19:45:43,259 - DEBUG - i is 3, total is 0
2019-05-29 19:45:43,260 - DEBUG - i is 4, total is 0
2019-05-29 19:45:43,260 - DEBUG - i is 5, total is 0
2019-05-29 19:45:43,261 - DEBUG - return value is 0
2019-05-29 19:45:43,262 - DEBUG - End of program
```

### Output of corrected version

```py
2019-05-29 19:47:33,273 - DEBUG - Start of factorial(5)
2019-05-29 19:47:33,274 - DEBUG - i is 1, total is 1
2019-05-29 19:47:33,274 - DEBUG - i is 2, total is 2
2019-05-29 19:47:33,275 - DEBUG - i is 3, total is 6
2019-05-29 19:47:33,275 - DEBUG - i is 4, total is 24
2019-05-29 19:47:33,276 - DEBUG - i is 5, total is 120
2019-05-29 19:47:33,276 - DEBUG - return value is 120
2019-05-29 19:47:33,277 - DEBUG - End of program
```

### 5 log levels

```py
- debug - logging.debug()
- info - logging.info()
- warning - logging.warning()
- error - logging.error()
- critical - logging.critical()
```

---

## 37: Using the Debugger

- Step Return - runs the line of code and moves to next line (including functions it calls)
- Step Into - runs the line of code and goes to next line to be run
- Breakpoints - force the execution to pause when it reaches that point
- In Spyder, highlight line and click Set/Clear Breakpoint (F12)

---
