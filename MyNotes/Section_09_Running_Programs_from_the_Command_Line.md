# Section 09: Running Programs from the Command Line

## 22: Launching Programs from Outside IDLE

Each python script should begin with a SheBang line: `#! /usr/bin/python3`

```bash
python3 pythonScript.py
```

`sys.argv` = information added when running from command line, in the form of a list
Such as

```bash
python3 pythonScript.py arg1 arg2

```

---
