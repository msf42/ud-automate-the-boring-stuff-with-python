# Section 10: Regular Expressions

## 23: Regular Expression Basics

### The Non-Regular Expression Way - Lots of code!

```py
def isPhoneNumber(text):
  if len(text) != 12:
    return False # not phone number size
  for i in range(0, 3):
    if not text[i].isdecimal():
      return False # no area code
  if text[3] != '-':
      return False #missing dash
  for i in range(4, 7):
    if not text[i].isdecimal():
      return False # no first 3 digits
  if text[7] != '-':
    return False # missing second dash
  for i in range(8,12):
    if not text[i].isdecimal():
      return False # missing last 4 digits
  return True

isPhoneNumber('865-236-5193')

isPhoneNumber('asdfasdf')

message = 'Call me at 833-445-3454 tomorrow or at 344-788-3493'
foundNumber = False
for i in range(len(message)):
  chunk = message[i:i+12]
  if isPhoneNumber(chunk):
    print('Phone Number Found: ' + chunk)
    foundNumber = True
if not foundNumber:
  print('Could not find any phone numbers')
```

### The Regular Expression Way - Much faster and shorter!

```py
import re      # Regular Expressions
message = 'Call me at 833-445-3454 tomorrow or at 344-788-3493'
phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d') # r = raw string; \d = digit
mo = phoneNumRegex.search(message)
# mo = phoneNumRegex.findall(message) will find all occurences
print(mo.group())

# output =
833-445-3454
```

---

## 24: Regex Groups and the Pipe Character

```py
import re
phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
phoneNumRegex.search('My number is 343-333-5654')
# results
# <_sre.SRE_Match object; span=(13, 25), match='343-333-5654'>

# as in previous lesson
mo = phoneNumRegex.search('My number is 343-333-5654')
mo.group()
# '343-333-5654'

phoneNumRegex = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)') # (grouping)
mo = phoneNumRegex.search('My number is 232-324-5555')
mo.group()
# '232-324-5555'
mo.group(1)
# '232'
# use \( if searching for literal (
```

### Pipe Character |

```py
batRegex = re.compile(r'Bat(man|mobile|bat)')
# searches for Batman, Batmobile, or Batbat

mo = batRegex.search('Batmobile lost a wheel')
mo.group()
# Out[14]: 'Batmobile'
```

---

## 25: Repetition in Regex Patterns and Greedy/Nongreedy Matching

### ? = 0 or 1

```py
import re

# ? = 0 or 1
batRegex = re.compile(r'Bat(wo)?man')

mo = batRegex.search('The Adventures of Batman')
mo.group()
Out[16]: 'Batman' # wo = 0

mo = batRegex.search('The Adventures of Batwoman')
mo.group()
Out[17]: 'Batwoman' # wo = 1

mo = batRegex.search('The Adventures of Batwossssasdfasdfasdf')
mo == None # because it wasn't found
Out[18]: True


phoneRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
mo = phoneRegex.search('My phone number is 344-345-9797')
mo.group()
Out[19]: '344-345-9797'


phoneRegex = re.compile(r'(\d\d\d)?-\d\d\d-\d\d\d\d') # ? makes area code optional
mo = phoneRegex.search('My phone number is 394-345-9797')
mo.group()
Out[20]: '394-345-9797
```

### = 0 or more, + = 1 or more

```py
batRegex = re.compile(r'Bat(wo)*man') #   * means wo can appear 0 or more times
# this would return True for 'Batwowowowoman'
#    + means 1 or more, so it would return None for 'Batman'
```

### \ is used for escape

```py
regex = re.compile(r'\+\*\?')
regex.search('I learned about +*? regex syntax')
# Out[23]: <_sre.SRE_Match object; span=(16, 19), match='+*?'>
```

### Specific number of times

```py
haRegex = re.compile(r'(ha){3}')
haRegex.search('He said hahaha')
Out[21]: <_sre.SRE_Match object; span=(8, 14), match='hahaha'>
```

### At least x, at most y

```py
haRegex = re.compile(r'(ha){3,6}')

haRegex.search('He said hahaha')
Out[35]: <_sre.SRE_Match object; span=(8, 14), match='hahaha'>

haRegex.search('He said hahahaha')
Out[36]: <_sre.SRE_Match object; span=(8, 16), match='hahahaha'>
```

### Greedy Match

```py
digitRegex = re.compile(r'(\d){3,5}')

digitRegex.search('1234567890')
Out[41]: <_sre.SRE_Match object; span=(0, 5), match='12345'>
```

### Non-greedy Match - will find the first match that meets the minimum search criteria

```py
digitRegex = re.compile(r'(\d){3,5}?')

digitRegex.search('1234567890')
Out[43]: <_sre.SRE_Match object; span=(0, 3), match='123'>
```

---

## 26: Regex Character Classes and the `findall()` Method

### With a single group, it returns a list of strings

```py
import re

phoneRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')

phoneRegex
Out[3]: re.compile(r'\d\d\d-\d\d\d-\d\d\d\d', re.UNICODE)

phoneRegex.findall('My number is 423-433-9889. Yours is 430-493-9888.')
Out[5]: ['423-433-9889', '430-493-9888']
```

### With 2 groups, it returns a list of tuples of strings

```py
phoneRegex.findall('My number is 423-433-9889. Yours is 430-493-9888.')
Out[7]: [('423', '433-9889'), ('430', '493-9888')]
```

### Character Classes

- \d = digits 0-9
- \D = not a digit
- \w = alphanumeric or underscore
- \W = not alphanumeric or underscore
- \s = space, tab, or newline character
- \S = not a space, tab, or newline character
- [aeiou]= custom character class, in this case, any vowel
- can also use [a-f], [2-4], etc
- `doubleVowelRegex = re.compile(r``'``[aeiouAEIOU]{2}``'``)`
- `notaVowelRegex = re.compile(r'[^aeiouAEIOU]')` ^negates values

---

## 27: Regex Dot-Star and the Carat/Dollar Characters

### ^ = must occur at beginning, $ = must occur at end

```py
In [1]: import re

In [2]: beginsWithHelloRegex = re.compile(r'^Hello')

In [3]: beginsWithHelloRegex.search('Hello there!')
Out[3]: <_sre.SRE_Match object; span=(0, 5), match='Hello'>

In [4]: beginsWithHelloRegex.search('I said hello there!')

In [5]: beginsWithHelloRegex.search('I said hello there!') == None
Out[5]: True

In [6]: endsWithWorldRegex = re.compile(r'world$')

In [7]: endsWithWorldRegex.search('Hello world')
Out[7]: <_sre.SRE_Match object; span=(6, 11), match='world'>

In [8]: endsWithWorldRegex.search('Hello people!') == None
Out[8]: True

In [9]: allDigitsRegex = re.compile(r'^\d+$')

In [10]: allDigitsRegex.search('4563456345654634')
Out[10]: <_sre.SRE_Match object; span=(0, 16), match='4563456345654634'>

In [11]: allDigitsRegex.search('4563456345654634x')

In [12]: allDigitsRegex.search('4563456345654634x') == None
Out[12]: True
```

### . = anything except new line

```py
In [13]: atRegex = re.compile(r'.at') # one character before 'at'

In [14]: atRegex.findall('The cat in the hat sat on the flat mat')
Out[14]: ['cat', 'hat', 'sat', 'lat', 'mat']

In [15]: atRegex = re.compile(r'.{1,2}at') # one or two chars before 'at'

In [16]: atRegex.findall('The cat in the hat sat on the flat mat')
Out[16]: [' cat', ' hat', ' sat', 'flat', ' mat']
```

### `.*` = match anything (`.*` is greedy, .*? is non-greedy)

```py
In [17]: nameRegex = re.compile(r'First Name: (.*) Last Name: (.*)')

In [18]: nameRegex.findall('First Name: Steve Last Name: Furches')
Out[18]: [('Steve', 'Furches')]

In [19]: serve = '<To serve humans> for dinner.>'

In [20]: nongreedy = re.compile(r'<(.*?)>')

In [21]: nongreedy.findall(serve)
Out[21]: ['To serve humans']

In [22]: greedy = re.compile(r'<(.*)>')

In [23]: greedy.findall(serve)
Out[23]: ['To serve humans> for dinner.']
```

### DOTALL

```py
In [28]: prime = 'Serve the public trust.\nProtect the innocent\nUphold the law'

In [29]: print(prime)
Serve the public trust.
Protect the innocent
Uphold the law

In [30]: dotStar = re.compile(r'.*')

In [31]: dotStar.search(prime)
Out[31]: <_sre.SRE_Match object; span=(0, 23), match='Serve the public trust.'>

In [32]: dotStar = re.compile(r'.*', re.DOTALL) # includes new line

In [33]: dotStar.search(prime)
Out[33]: <_sre.SRE_Match object; span=(0, 59), match='Serve the public trust.\nProtect the innocent\nUp>
```

### re.IGNORECASE

```py
In [28]: prime = 'Serve the public trust.\nProtect the innocent\nUphold the law'

In [29]: print(prime)
Serve the public trust.
Protect the innocent
Uphold the law

In [30]: dotStar = re.compile(r'.*')

In [31]: dotStar.search(prime)
Out[31]: <_sre.SRE_Match object; span=(0, 23), match='Serve the public trust.'>

In [32]: dotStar = re.compile(r'.*', re.DOTALL)

In [33]: dotStar.search(prime)
Out[33]: <_sre.SRE_Match object; span=(0, 59), match='Serve the public trust.\nProtect the innocent\nUp>'

In [35]: vowelRegex = re.compile(r'[aeiou]', re.IGNORECASE)

In [37]: vowelRegex.findall(prime)
Out[37]:
['e',
'e',
'e',
'u',
'i',
'u',
'o',
'e',
'e',
'i',
'o',
'e',
'U',
'o',
'e',
'a']
```

---

## 28: Regex `sub()` Method and Verbose Mode

### The `sub()` Method

```py
In [1]: import re

In [2]: namesRegex = re.compile(r'Agent \w+')

In [3]: namesRegex.findall('Agent Alice gave the secret documents to Agent Bob')
Out[3]: ['Agent Alice', 'Agent Bob']

In [4]: namesRegex.sub('REDACTED', 'Agent Alice gave the secret documents to Agent Bob')
Out[4]: 'REDACTED gave the secret documents to REDACTED'
```

### \1, \2, etc in** `sub()`

```py
In [5]: namesRegex = re.compile(r'Agent (\w)\w*')

In [6]: namesRegex.findall('Agent Alice gave the secret documents to Agent Bob')
Out[6]: ['A', 'B']

In [7]: namesRegex.sub(r'Agent \1*****', 'Agent Alice gave the secret documents to Agent Bob')
Out[7]: 'Agent A***** gave the secret documents to Agent B*****'
```

### Verbose Mode with re.VERBOSE

```py
re.compile(r'''
  (\d\d\d) |    # area code (without parens, with dash)
  (\(\d\d\d\) ) # -or- area code with parens and no dash
  \d\d\d        # first 3 digits
  -             # second dash
  \d\d\d\d      # last 4 digits
  \sx\d(2,4)    # extension like x1234''', re.VERBOSE)
```

### Using Multiple Options

```py
re.IGNORECASE | re.DOTALL
```

---

## 29: Regex Example Program: A Phone and Email Scraper

```py
#! /usr/bin/python3

import re, pyperclip

# Create a regex for phone numbers
phoneRegex = re.compile(r'''
(
# 415-555-0000, 222-2222, (543) 333-3333, 222-3333 ext 12345, ext.22222, x23433
((\d\d\d) | (\(\d\d\d\)))?        # area code (optional)
(\s|-)              # first separator
\d\d\d              # first 3 digits
-                   # separator
\d\d\d\d            # last 4 digits
(((ext(\.)?\s) |x)  # extension word (optional)
(\d{2,5}))?         # extension number (optional)
)
''', re.VERBOSE)

# Create a regex for email addresses
emailRegex = re.compile(r'''
[a-zA-Z0-9_.+]+          # name part
@                        # symbol
[a-zA-Z0-9_.+]+          # domain name
''', re.VERBOSE)


# Get the text off the clipboard
text = pyperclip.paste()

# TODO: Extract teh email/phone from the text
extractedPhone = phoneRegex.findall(text)
extractedEmail = emailRegex.findall(text)

allPhoneNumbers = []
for phoneNumber in extractedPhone:
  allPhoneNumbers.append(phoneNumber[0])

# TODO: Copy the extracted email/phone to the clipboard
results = '\n'.join(allPhoneNumbers) + '\n' + '\n'.join(extractedEmail)

pyperclip.copy(results)
```

---
