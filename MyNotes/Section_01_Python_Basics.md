# Section 01: Python Basics

## 01: Getting Python Installed

Just basic intro. Installed IDLE and set up files.

---

## 02: Basic Terminology & Using IDLE

### Expressions

- Expressions = Values + Operators (`2 + 2`)
- Expressions always *evaluate*, meaning they always reduce to a single value - no matter how big it starts out as.

### Data Types

- integers, floats, strings, string concatenation, string replication

### Variables

- variables, variable reassignment

Whereas expressions are evaluated, statements (a.k.a. instructions or code) are not evaluated

---

## 03: Writing Our First Program

“The Execution” = Which instruction is currently being executed

Terms covered: `print` function, `len` function, `input` function, functions, `int`, `str`, and `float`

### Code

```py
# This program says hello and asks for my name

print('Hello World!')

print('What is your name?')
myName = input()
print('It is good to meet you, ' + myName)
print('The length of your name is:')
print(len(myName))

print('What is your age?')
myAge = input()
print('You will be ' + str(int(myAge) + 1) + ' next year')
```

### Output

```py
Hello World!
What is your name?
Steve
It is good to meet you, Steve
The length of your name is:
5
What is your age?
42
You will be 43 next year
```

---
