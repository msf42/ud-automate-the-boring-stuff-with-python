# Section 16: GUI Automation

## 48: Controlling the Mouse from Python

The **origin** of the screen is the top left, so

- X increases to the right
- Y increases down

```py
In [1]: import pyautogui

In [2]: pyautogui.size()
Out[2]: Size(width=1366, height=768)

In [3]: pyautogui.position()
Out[3]: Point(x=639, y=467)

In [4]: pyautogui.position()
Out[4]: Point(x=35, y=54)

In [5]: pyautogui.position()
Out[5]: Point(x=1333, y=712)

In [6]: pyautogui.moveTo(10, 10)

In [8]: pyautogui.moveTo(10, 10, duration=2)

In [9]: pyautogui.moveRel(20, 0)

In [10]: pyautogui.moveRel(-200, 0)

In [11]: pyautogui.click(10, 10)
- `.doubleclick` and `.middleclick` also
- `.dragTo` and `.dragRel` too
- `pyautogui.displayMousePosition()` only works in command line
```

---

## 49: Controlling the Keyboard from Python

`pyautogui.keyboard_KEYS()` returns a list of all keys

```py
In [1]: import pyautogui

In [2]: pyautogui.click(754, 312); pyautogui.typewrite('Hello World')

In [3]: pyautogui.click(754, 312); pyautogui.typewrite('Hello World', interval=0.2)

In [5]: pyautogui.click(754, 312); pyautogui.typewrite(['a', 'b', 'left', 'left', 'x', 'y'], interval=0.2)
```

---

## 50: Screenshots and Image Recognition

```py
In [1]: import pyautogui

In [2]: pyautogui.screenshot()
Out[2]: # screenshot

In [3]: # pyautogui.screenshot('example.png')

In [4]: # pyautogui.locateOnScreen('filename.png')

In [5]: # returns XY coord & W/H of image on screen

In [6]: # pyautogui.locateCenterOnScreen
```

---
