# Section 04: Handling Errors with Try/Except

## 11: Try/Except

Just a review of try/except. Great for input validation

```py
try:
    do this
except: ErrorMessageHere:
    do this
```

---
