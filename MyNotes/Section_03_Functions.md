# Section 03: Functions

## 8: Python’s Built-In Functions

### Standard Functions and Modules

In addition to **standard functions** such as `print()`, `len()`, etc, Python comes with a library of **modules** that can be called, such as `math`, `random`, etc.

- these must be imported, such as

```py
import random
random.randint(1, 10)
```

If you write `from random import *`, you don’t need to include `random.` in front of `randint`

```py
import sys
sys.exit() # this will terminate the program
```

### Third Party Modules

- PIP program

### pyperclip.copy(), pyperclip.paste()

```py
pyperclip.copy("Hello World")
pyperclip.paste()
'Hello World'
```

---

## 9: Writing Your Own Functions

General review of functions.

```py
def function(parameter);
function(argument)
```

- **Argument =** The value passed in the function call
- **Parameter =** The variable inside the function
- **De-duplication** = Getting rid of duplicated code

Every function call has a return value, even `print`

- The **None** data type can be valuable as a placeholder
- **Print** automatically adds a new line at the end
- We can add

```py
print(``"``Hello``"``, end=``'` ```'``)
```

to make a space instead of a return

- If we print multiple strings, as in

```py
print(``"``Hello``"``,` `"``World``"``)`
```

it will automatically add a space between the strings

- But we can add `, sep =` `'``xxxxx``'` to change the space to whatever we like

---

## 10: Global and Local Scopes

Pretty standard review of global and local variables

---
