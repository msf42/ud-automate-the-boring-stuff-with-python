# Section 07: Dictionaries

## 17: The Dictionary Data Type

```py
myCat = {'size': 'fat', 'color': 'gray', 'disposition':'loud'}
myCat['size']
'fat'
'My cat has ' + myCat['color'] + ' fur.'
'My cat has gray fur.'

spam = {12345: "luggage combination", 42: "The Answer"}

spam
{12345: 'luggage combination', 42: 'The Answer'}

'luggage combination' in spam
False
12345 in spam
True

# Dictionaries are mutable, like lists
# Dictionaries are unordered
list(spam.values())
['luggage combination', 'The Answer']
list(spam.keys())
[12345, 42]
list(spam.items())
[(12345, 'luggage combination'), (42, 'The Answer')]

for k in spam.keys():
  print(k)
12345
42

for v in spam.values():
  print(v)
luggage combination
The Answer

for k, v in spam.items():
  print(k)
  print(v)
12345
luggage combination
42
The Answer

spam
{12345: 'luggage combination', 42: 'The Answer'}

spam.get(42, 0) # returns 0 if item doesn't exist
'The Answer'

# Set Default Method
spam
{12345: 'luggage combination', 42: 'The Answer'}

if 'name' not in spam:
  spam['name'] = 'steve'

spam
{12345: 'luggage combination', 42: 'The Answer', 'name': 'steve'}

import pprint # pretty print

message = 'It was a bright cold day in April and the clocks were striking thirteen'

count = {}

for character in message.upper():
  count.setdefault(character, 0)
  count[character] = count[character] + 1

print(count)

======================== RESTART: /home/steve/test.py ========================
{'I': 1, 't': 6, ' ': 13, 'w': 2, 'a': 4, 's': 3, 'b': 1, 'r': 5, 'i': 6, 'g': 2, 'h': 3, 'c': 3, 'o': 2, 'l': 3, 'd': 3, 'y': 1, 'n': 4, 'A': 1, 'p': 1, 'e': 5, 'k': 2}

======================== RESTART: /home/steve/test.py ========================
{'I': 7, 'T': 6, ' ': 13, 'W': 2, 'A': 5, 'S': 3, 'B': 1, 'R': 5, 'G': 2, 'H': 3, 'C': 3, 'O': 2, 'L': 3, 'D': 3, 'Y': 1, 'N': 4, 'P': 1, 'E': 5, 'K': 2}
```

---

## 18: Data Structures

General review of how to use dictionaries, how to change them, etc.

```py
In [1]: cat = {'name':'sophie', 'age':7, 'color':'gray'}

In [2]: allCats = []

In [3]: allCats.append({'name':'sophie', 'age':7, 'color':'gray'})

In [4]: allCats.append({'name':'Pookah', 'age':5, 'color':'black'})

In [5]: allCats.append({'name':'fat-tail', 'age':5, 'color':'gray'})

In [6]: allCats.append({'name':'???', 'age':-1, 'color':'orange'})

In [7]: allCats
Out[7]:
[{'age': 7, 'color': 'gray', 'name': 'sophie'},
{'age': 5, 'color': 'black', 'name': 'Pookah'},
{'age': 5, 'color': 'gray', 'name': 'fat-tail'},
{'age': -1, 'color': 'orange', 'name': '???'}]


In [8]: theBoard = {'top-L': ' ', 'top-M': ' ','top-R': ' ','mid-L': ' ','mid-M': ' ','mid-R': ' ','low-L': ' ','low-M': ' ', 'low-R': ' '}

In [9]: theBoard
Out[9]:
{'low-L': ' ',
'low-M': ' ',
'low-R': ' ',
'mid-L': ' ',
'mid-M': ' ',
'mid-R': ' ',
'top-L': ' ',
'top-M': ' ',
'top-R': ' '}

In [12]: def printBoard(board):
  ...: print(board['top-L'] + '|' + board['top-M'] + '|' + board['top-R'])
  ...: print('-----')
  ...: print(board['mid-L'] + '|' + board['mid-M'] + '|' + board['mid-R'])
  ...: print('-----')
  ...: print(board['low-L'] + '|' + board['low-M'] + '|' + board['low-R'])
  ...:

In [13]: printBoard(theBoard)
| |
-----
| |
-----
| |

Type function

In [14]: type(42)
Out[14]: int

In [15]: type('hello')
Out[15]: str

In [16]: type(theBoard)
Out[16]: dict
```

---
