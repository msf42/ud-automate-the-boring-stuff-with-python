# Section 06: Lists

## 13: The List Data Type

- **Items** = parts of a list
- **Slice** = list of values

A pretty straightforward review of lists

```py
spam = ['cat', 'bat', 'rat', 'elephant']
spam[0]
'cat'
spam = [['cat', 'bat'], [10, 20, 30, 40, 50]]
spam[0]
['cat', 'bat']
spam\[0\][1]
'bat'
spam\[1\][4]
50
spam\[1\][-2]
40
spam = ['cat', 'bat', 'rat', 'elephant']
'The ' + spam[-1] + ' is afraid of the ' + spam[-3] + '.'
'The elephant is afraid of the bat.'
spam[1:3]
['bat', 'rat']

spam = "Hello"
spam
'Hello'
spam = [10, 20, 30]
spam[1] = "Hello"
spam
[10, 'Hello', 30]
spam[1:3] = ['CAT', 'DOG', 'MOUSE']
spam
[10, 'CAT', 'DOG', 'MOUSE']

spam = ['cat', 'bat', 'rat', 'elephant']
spam[:2]
['cat', 'bat']
spam[1:]
['bat', 'rat', 'elephant']

spam
['cat', 'bat', 'rat', 'elephant']
del spam[2]
spam
['cat', 'bat', 'elephant']

len('Hello')
5
len(spam)
3
len("hello" * 3)
15

list('Hello')
['H', 'e', 'l', 'l', 'o']

spam
['cat', 'bat', 'elephant']
'cat' in spam
True
'rat' in spam
False
```

---

## 14: For Loops with Lists, Multiple Assignment, and Augmented Operators

For loops run through lists or list-like objects

- Python actually reads `range(5)` as a list, as `list(range(5))` , or `[0, 1, 2, 3, 4]`

```py
supplies = ['pens', 'staples', 'flame-throwers', 'binders']
for i in range(len(supplies)):
  print('Index ' + str(i) + ' in supplies is ' + supplies[i])

Index 0 in supplies is pens
Index 1 in supplies is staples
Index 2 in supplies is flame-throwers
Index 3 in supplies is binders

# one way of doing it...
cat = ['fat', 'orange', 'loud']

# then assigning each to a variable
size = cat[0]
color = cat[1]
disposition = cat[2]

# instead, we could just do this:
size, color, disposition = cat
# this will assign 'fat' to size, etc

# could also do this:
size, color, disposition = 'skinny', 'black', 'quiet'
cat
['fat', 'orange', 'loud']
size
'skinny'
color
'black'
disposition
'quiet'

# swapping variables
a = 'AAA'
b = 'BBB'
a, b = b, a
a
'BBB'
b
'AAA'

# augmented assignment
spam = 41
spam += 1
spam
42
```

---

## 15: List Methods

Mostly things covered in last course

```py
spam = ['hello', 'hi', 'howdy', 'heyas']
spam.index('hello')
0
# if there are multiple values, it returns the first


# append adds to end, insert goes at the index you give
spam.append('steve')
spam
['hello', 'hi', 'howdy', 'heyas', 'steve']
spam.insert(2, "furches")
spam
['hello', 'hi', 'furches', 'howdy', 'heyas', 'steve']


# remove method
spam.remove('howdy')
spam
['hello', 'hi', 'furches', 'heyas', 'steve']
del spam[3]
spam
['hello', 'hi', 'furches', 'steve']
# remove uses a value, del uses an index


# .sort can be used to sort numbers or strings in a list
spam.sort(reverse=True)
spam
['steve', 'hi', 'hello', 'furches']
# ASCI-betical order = upper case first; ABC....Zabc...xyz
spam.sort(key=str.lower) # sorts in true alphabetical order
```

---

## 16: Similarities Between Lists and Strings

```py
list('Hello')
['H', 'e', 'l', 'l', 'o']
name = 'Sophie'
name[0]
'S'
name[1:3]
'op'
# strings and lists have similar functions, but strings are immutable


# the proper way to modify a string is to make a new string via slices

name = 'Sophie a cat'
newName = name[0:7] + 'the' + name[8:12]
newName
'Sophie the cat'


spam = [0, 1, 2, 3, 4, 5]
cheese = spam
cheese[1] = 'hello'
cheese
[0, 'hello', 2, 3, 4, 5]
spam
[0, 'hello', 2, 3, 4, 5]
# this is a reference to the same list
# cheese isn't a copy of spam, but a reference to the same list
# this applies to any mutable value, becuase mutable values are replaced, rather than modified

import copy
# this creates a new copy of the list
spam = ['A', 'B', 'C', 'D']
cheese = copy.deepcopy(spam)
del spam[2]
spam
['A', 'B', 'D']
cheese
['A', 'B', 'C', 'D']


# line continuation
spam = ['apples',
    'oranges',
    'bananas']
spam
['apples', 'oranges', 'bananas']
print('Four score and seven' + \
    ' years ago')
Four score and seven years ago
```

---
